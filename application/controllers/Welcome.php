<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function general_info(){
		$awb = $this->input->post('data_awb');

		if($awb == "SJ123"){
			$data['status'] = 1;
			$data['awb'] = $awb;
			$data['service'] = 'REG';
			$data['date_shipment'] = '19-02-2018 19:11:09';
			$data['origin'] = 'SURABAYA';
			$data['destination'] = 'BALIKPAPAN';

			$data['shipper_name'] = 'SHP CARGO';
			$data['shipper_address'] = 'Jln Gresik, Surabaya';
			$data['consignee_name'] = 'TOKO BUDIMAN';
			$data['consignee_address'] = 'Jln Panglima Sudirman Balikpapan';

			$data['data'][0] = array(
					"action" => "Paket telah di input (manifested) di Depok [Beji Timur]",
					"date" => "2018-07-14 19:41"
				);
			$data['data'][1] = array(
					"action" => "Paket telah di terima di Surabaya [Gubeng Kertajaya]",
					"date" => "2018-07-17 07:51"
				);
			$data['data'][2] = array(
					"action" => "Paket dibawa [SIGESIT - Nurcahyo]",
					"date" => "2018-07-17 07:51"
				);

		}else{
			$data['status'] = 0;
		}
		echo json_encode($data);

	}

	public function city(){
		$data['data'][0] = array(
			'id_city' => '1',
			'city' => 'SURABAYA' 
		);
		$data['data'][1] = array(
			'id_city' => '2',
			'city' => 'JAKARTA' 
		);
		$data['data'][2] = array(
			'id_city' => '1',
			'city' => 'SURABAYA' 
		);
		$data['data'][3] = array(
			'id_city' => '1',
			'city' => 'SURABAYA' 
		);
		$data['data'][4] = array(
			'id_city' => '1',
			'city' => 'SURABAYA' 
		);

		echo json_encode($data);
	}

	public function destination(){
		$data['data'][0] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);
		$data['data'][1] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);
		$data['data'][2] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);
		$data['data'][3] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);
		$data['data'][4] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);
		$data['data'][5] = array(
			'id_destination' => '1',
			'destination' => 'SURABAYA' 
		);

		echo json_encode($data);
	}

	public function post_tarif(){

	}

	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($username == 'shp_cargo' && $password == "shp_cargo"){
			$data = 1;
		}else{
			$data = 0;
		}
		// var_dump($data);

		echo json_encode($data);
	}
}

